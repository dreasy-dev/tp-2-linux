# Partie 1 : SSH

- [Partie 1 : SSH](#partie-1--ssh)
- [I. Intro](#i-intro)
- [II. Setup du serveur SSH](#ii-setup-du-serveur-ssh)
  - [1. Installation du serveur](#1-installation-du-serveur)
  - [2. Lancement du service SSH](#2-lancement-du-service-ssh)
  - [3. Etude du service SSH](#3-etude-du-service-ssh)
  - [4. Modification de la configuration du serveur](#4-modification-de-la-configuration-du-serveur)

![Feels like a hacker](./pics/feels_like_a_hacker.jpg)



🌞 **Installer le paquet `openssh-server`**

- avec une commande `apt install`
```bash
apt install openssh-server -y 
```

---

Note : une fois que le paquet est installé, plusieurs nouvelles choses sont dispos sur la machine. Notamment :

- un service `sshd`
- un dossier de configuration `/etc/ssh/`

## 2. Lancement du service SSH

🌞 **Lancer le service `sshd`**

- avec une commande `systemctl start`
- vérifier que le service est actuellement actif avec une commande `systemctl status`
```bash
 State: running
```  

> Vous pouvez aussi faire en sorte que le *service* SSH se lance automatiquement au démarrage avec la commande `systemctl enable sshd`.

## 3. Etude du service SSH

🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du *service*
  - avec une commande `systemctl status`
```bash
 State: running
```  
- afficher le/les processus liés au *service* `sshd`
  - avec une commande `ps`
  - isolez uniquement la/les ligne(s) intéressante(s) pour le rendu de TP
```bash
 ps -e 
  1294 ?        00:00:00 sshd                                     
  1372 ?        00:00:00 sshd 
```  
- afficher le port utilisé par le *service* `sshd`
  - avec une commande `ss -l`
  - isolez uniquement la/les ligne(s) intéressante(s)
```bash
  LISTEN   ssh        128                                            0.0.0.0:22  
```
- afficher les logs du *service* `sshd`
  - avec une commande `journalctl`
  - en consultant un dossier dans `/var/log/`
```bash
 Using system hostname 'dreasy-VirtualBox'.   
```  
Cette ligne nous renseigne sur le nom de la machine.

  - ne me donnez pas toutes les lignes de logs, je veux simplement que vous appreniez à consulter les logs

---

🌞 **Connectez vous au serveur**

- depuis votre PC, en utilisant un **client SSH**

> *La commande `ssh` de votre terminal, c'est un client SSH.*
```bash
ssh dreasy@192.168.56.111 
dreasy's' password: ****
```

## 4. Modification de la configuration du serveur

Pour modifier comment un *service* se comporte il faut modifier le fichier de configuration. On peut tout changer à notre guise.

🌞 **Modifier le comportement du service**

- c'est dans le fichier `/etc/ssh/sshd_config`
  - c'est un simple fichier texte
```bash
Include /etc/ssh/sshd_config.d/*.conf                                                                                                                                                                                                           #Port 1080                                                                                                               #AddressFamily any                                                                                                      #ListenAddress 0.0.0.0                                                                                                  #ListenAddress ::
```
  
- effectuez le modifications suivante :
  - changer le ***port d'écoute*** du service *SSH*
    - peu importe lequel, il doit être compris entre 1025 et 65536
  - vous me prouverez avec un `cat` que vous avez bien modifié ces deux lignes
- pour cette modification, prouver à l'aide d'une commande qu'elle a bien pris effet
```bash
dreasy@dreasy-VirtualBox:~$ cat /etc/ssh/sshd_config                                                                    #       $OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $                                                                                                                                                                              # This is the sshd server system-wide configuration file.  See                                                          # sshd_config(5) for more information.                                                                                                                                                                                                          # This sshd was compiled with PATH=/usr/bin:/bin:/usr/sbin:/sbin                                                                                                                                                                                # The strategy used for options in the default sshd_config shipped with                                                 # OpenSSH is to specify options with their default value where                                                          # possible, but leave them commented.  Uncommented options override the                                                 # default value.                                                                                                                                                                                                                                Include /etc/ssh/sshd_config.d/*.conf                                                                                                                                                                                                           #Port 1080                                                                                                              #AddressFamily any                                                                                                      #ListenAddress 0.0.0.0                                                                                                  #ListenAddress :: 
```   
  - une commande `ss -l` pour vérifier le port d'écoute
```bash
dreasy@dreasy$ ss -lpn
tcp     LISTEN   ssh        128                                       0.0.0.0:1080   
```

> Vous devez redémarrer le service avec une commande `systemctl restart` pour que les changements inscrits dans le fichier de configuration prennent effet.

🌞 **Connectez vous sur le nouveau port choisi**

- depuis votre PC, avec un *client SSH*

```cmd
ssh dreasy@192.168.56.111
password for dreasy: 
```



